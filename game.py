users_name = input("Hi! What is your name? ")


# for guess in range(5):
# Guess 1

for guess_number in range(5):
    from random import randint
    year_guess = randint(1924,2004)
    month_guess = randint(1,12)

    print("Guess", guess_number + 1, ":",  users_name, "were you born in", month_guess, "/", year_guess, "?" )
    guess_check = input("Yes or No?")

    if guess_check == "Yes":
        print("I knew it!")
        exit()
    elif guess_number < 4:
        print("Drat! Lemme try again!")

print("I don't have time for this.")
